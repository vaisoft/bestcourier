package com.bestcourier

import scala.collection.mutable.ListBuffer

object BestPriceCalculator {

  val bobby = CourierDetails("Bobby", 9, 13, 5, true, 1.75)
  val martin = CourierDetails("Martin", 9,17, 3, false, 1.50)
  val geoff = CourierDetails("Geoff", 10, 16,4, true, 2.00 )

  val courierCompanyList = Array(bobby, martin, geoff)

  def findBestCourier(timeOfTheDay : Int, distance : Int, isRefrigeratorRequired : String):Unit = {
    println(s"Your Inputs are TimeOfDay: $timeOfTheDay Distance: $distance isRefrigerationRequired: $isRefrigeratorRequired")
    val buffer = new ListBuffer[PriceOfCourier]()

    for (courierCoompany <- courierCompanyList) {

      if (isRefrigeratorRequired.equalsIgnoreCase("Y")) {
        if (courierCoompany.hasRefrigerator && (courierCoompany.workStartTime to courierCoompany.workEndTime contains (timeOfTheDay))) {
          buffer += PriceOfCourier(courierCoompany.courierPerson, distance * courierCoompany.charges)
        }

      } else {
        if (courierCoompany.workStartTime to courierCoompany.workEndTime contains (timeOfTheDay)) {
          buffer += PriceOfCourier(courierCoompany.courierPerson, distance * courierCoompany.charges)
        }
      }
    }

    findCheapest(buffer)

  }

  private def findCheapest(buffer: ListBuffer[PriceOfCourier]) = {
    if (buffer.nonEmpty) {
      val bestCourier = buffer.reduceLeft(max)
      println(s" The Best Courier is: ${bestCourier.courierPerson} With the Price:£${bestCourier.totalCost}")

    } else {
      println("No Courier available for your given time or preferences, please try again")
    }
  }

  def max(p1: PriceOfCourier, p2: PriceOfCourier): PriceOfCourier = if (p1.totalCost > p2.totalCost) p1 else p2

  def isInputType(value: Any) : Boolean = {
    val INT_PATTERN: String = "^[-+]?[0-9]*$"
    if(value.toString.matches(INT_PATTERN)) return true
    false
  }

}
