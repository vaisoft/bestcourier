package com.bestcourier

import scala.io.StdIn.readLine
import scala.util.{Failure, Success, Try}

/**
 * @author Daya K Dubey
 */
object App {



  def main(args : Array[String]) {
    println( "Application Started...." )

    print("Please enter time (1-24) order to be shipped:")
    val timeOfTheDay = readLine()
    if( !BestPriceCalculator.isInputType(timeOfTheDay)){
      println("You can only enter numeric values. Please try again")
      System.exit(0)
    }

    print("Please enter distance as number:")
    val distance = readLine()
    if( !BestPriceCalculator.isInputType(distance)){
      println("You can only enter numeric values. Please try again")
      System.exit(0)
    }

    print("Please enter Y or N if refrigeration is required:")
    val isRefrigerationRequired = readLine()

    BestPriceCalculator.findBestCourier(timeOfTheDay.toInt, distance.toInt, isRefrigerationRequired)

    println( "Application Exited...." )

  }

}
